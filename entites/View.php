<?php
abstract class View
{
    public $storage;

    abstract function displayTextById($id);

    abstract function displayTextByUrl($url);
}