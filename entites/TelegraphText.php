<?php
class TelegraphText
{
    private $text;
    private $title;
    private $author;
    private $published;
    private $slug;

    public function __construct($author, $slug)
    {
        $this->author = $author;
        $this->published = date("Y-m-d H:i:s");
        $this->slug = $slug;
    }

    private function storeText()
    {
        $array = ['title' => $this->title, 'text' => $this->text, 'author' => $this->author, 'published' => $this->published];
        file_put_contents($this->slug, serialize($array));
    }

    private function loadText()
    {
        $file = file_get_contents($this->slug);
        $file = unserialize($file);
        $this->title = $file['title'];
        $this->text = $file['text'];
        $this->author = $file['author'];
        $this->published = $file['published'];

        return $file['text'];
    }

    public function editText($title, $text)
    {
        $this->text = $text;
        $this->title = $title;
    }

    public function __set($name, $value)
    {
        if ($name == 'text') {
            $this->text = $value;
            if (strlen($this->text < 500) && strlen($this->text > 0) ) {
                throw new \mysql_xdevapi\Exception('Текст меньше 500 знаков');
            }
            $this->storeText();
        }

        if ($name == 'author') {
            if (strlen($this->author) > 120) {
                echo 'строка больше 120 символов' . PHP_EOL;
            }

            return $this->author = $value;

        } elseif ($name == 'slug') {
            $pattern = '/^[a-zA-Z0-9]+$/i';
            if (preg_match($pattern, $value) == true) {
                echo 'Cтрока не соотвествует формату!' . PHP_EOL;
            }

            return $this->slug = $value;

        } elseif ($name == 'published') {
            if (date('dmY') <= $value) {
                echo 'Дата не соотвествует формату!' . PHP_EOL;
            }

            return $this->published = $value;
        }
    }

    public function __get($name)
    {
        if ($name == 'text') {

            return $this->loadText();
        }
        if ($name == 'author') {

            return $this->author;
        }
        if ($name == 'published') {

            return $this->published;
        }
        if ($name == 'slug') {

            return $this->slug;
        }
        if ($name == 'title') {

            return $this->title;
        }
    }
}
