<?php
require_once __DIR__ . '/' . "../interfaces/EventListenerInterface.php";

abstract class User
{
protected $id;
protected $name;
protected $role;

abstract function getTextsToEdit();

abstract function attachEvent($nameFunction,$callbackFunction);

abstract function detouchEvent($nameFunction);
}