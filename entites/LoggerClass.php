<?php
class LoggerClass implements LoggerInterface
{
    public function logMessage($textLog)
    {
        error_log($textLog,0);
    }

    public function lastMessages($countLog, $list)
    {
        $lastcountlog = [];
        $list = array_reverse($list);
        $i = 0;
        foreach ($list as $log) {
            if ($i !== $countLog) {
                $i++;
                array_push($lastcountlog, $log);
            }
        }

        return $lastcountlog;
    }

}