<?php
require_once __DIR__ . '/' . "../interfaces/LoggerInterface.php";
require_once __DIR__ . '/' . "../interfaces/EventListenerInterface.php";


abstract class Storage
{
    public $textLog;
    public $countLog;

    //abstract function logMessage($textLog);

    //abstract function lastMessages($countLog, $list);

    //abstract function attachEvent($nameFunction,$callbackFunction);

    //abstract function detouchEvent($nameFunction);

    abstract function create($telegraphText);

    abstract function read($slug);

    abstract function update($slug, $telegraphText);

    abstract function delete($slug);

    abstract function list();
}