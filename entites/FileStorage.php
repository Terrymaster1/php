<?php
class FileStorage extends Storage
{
    public function create($telegraphText)
    {
        $filename = $telegraphText->slug . '_' . date('dmY');
        $i = 1;
        while (file_exists($filename)) {
            $filename = $telegraphText->slug . '_' . date('dmY') . '_' . $i;
            $i++;
        }
        $telegraphText->slug = $filename;
        file_put_contents($telegraphText->slug, serialize($telegraphText));

        return $telegraphText->slug;
    }

    public function read($slug)
    {
        if (file_exists($slug)) {
            $telegraphText = unserialize(file_get_contents($slug));
        }

        return $telegraphText;
    }

    public function update($slug, $telegraphText)
    {
        if (file_exists($telegraphText->slug)) {
            file_put_contents($slug, serialize($telegraphText));
        }
    }

    public function delete($slug)
    {
        if (file_exists($slug)) {
            unlink($slug);
        }
    }

    public function list()
    {
        $files = array_diff(scandir(__DIR__), ['..', '.']); // оставляем все кроме этих папок . ..
        $list = [];
        foreach ($files as $file) {
            $dir = __DIR__ . '/' . $file;
            array_push($list, unserialize(file_get_contents($dir)));
        }

        return $list;
    }
}
