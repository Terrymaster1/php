<?php
function loader($className)
{
    if (file_exists(__DIR__ . '/' . 'entites/' . $className . '.php')) {
        require_once __DIR__ . '/' . 'entites/' . $className . '.php';
    }
    if (file_exists('./vendor/autoload.php')) {
        require_once "./vendor/autoload.php";
    }
}
spl_autoload_register('loader');