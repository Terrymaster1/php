<?php
interface LoggerInterface
{
    public function logMessage($textLog);
    public function lastMessages($countLog, $list);
}